1. Объект проверки.
   Набор тестов TestOrange проверяeт функциональность UI "OrangeHRM" (https://opensource-demo.orangehrmlive.com). Проводится проверка работы различных действий на разных страницах в сооветствии с тестовым заданием (TestTask.md).


2. В проект используются следующие библиотеки:

   MAVEN

   TESTNG

   LOMBOK
 
   SELENIDE

   SELENOID

   FAKER

   ALLURE

   ASPECTJ

   ALLURE PLUGIN

   MAVEN SUREFIRE PLUGIN

   GITLAB-CI


3. Запуск проекта.

   Запуск тестового сценария осуществляется командой 'mvn clean test'.

   Для формирования Allure Report необходимо вызвать команду 'mvn allure:report'.

   Для запуска полученного Allure Report в веб сервисе небходимо вызвать команду 'mvn allure:serve'.


4. Ожидаемые падения тестов.

   Крайне редко, но бывает не находит элементы username, password, searchField, при чем в большей части на уровне запуска pipeline, проблема явно внешняя, т.к. банальный перзапуск решает проблему, а все проверки не находят ошибок. 

   
