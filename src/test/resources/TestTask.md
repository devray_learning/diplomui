1. Открыть https://opensource-demo.orangehrmlive.com
2. Ввести логи и пароль
3. Ввести в поле поиска слово "time"
4. Переключиться на вкладку time
5. Ввести фейк имя
6. Нажать view
7. Не должно ничего произойти (кнопка view должна остаться)
8. Проверить появление красной надписи invalid 
9. Ввести в поле имя сотрудника из шапки
10. Нажать view 
11. Не должно ничего произойти (кнопка view должна остаться)
12. Еще раз вести в поле имя сотрудника из шапки 
13. Кликнуть на вариант автокомлита из выпадающего списка 
14. Нажать кнопку view 
15. Отфиксировать переход на другую страницу (кнопка view должна исчезнуть) 
16. Переключиться на вкладку MyInfo 
17. Ввести ФИО сотрудника "Test 63, Brown, Collings"
18. Нажать кнопку save 
19. Проверить наличие зеленого баннера об успешном сохранении 
20. Переключиться на влкдаку PIM 
21. Выбрать четырех сотрудников в списке
22. Проверить появление кнопки delete selected
23. Нажать кнопку delete selected 
24. Проверить появление подтверждающего запроса 
25. Нажать кнопку yes

