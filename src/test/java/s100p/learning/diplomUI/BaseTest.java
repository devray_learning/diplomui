package s100p.learning.diplomUI;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import s100p.learning.diplomUI.supportive.PageObjectSupplier;

import java.util.Map;

import static com.codeborne.selenide.Selenide.open;

public class BaseTest implements PageObjectSupplier {
    @BeforeSuite
    public void baseTest()  {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));
        Configuration.remote = " http://51.250.123.179:4444/wd/hub";
        //настройка для ускоренной работы теста, страницы не грузяется полностью, а обходятся прогрузкой структуры дома
        Configuration.pageLoadStrategy = "eager";
        Configuration.browserSize = "1920x1080";
        Configuration.browserCapabilities = capabilities;
        open("https://opensource-demo.orangehrmlive.com");
        //либо можно вынести в BeforeMethod класса запуска
        loginPage().logIn();
    }

    //открытие нужной вкладки перед группой тестов, которые работают на этой вкладке (3,4,5)
    @BeforeGroups("searchEmployee")
    public void selectTimeTab() {
        searchTimeTabPage().selectTimeTab();
    }

    //открытие нужной вкладки перед группой тестов, которые работают на этой вкладке (6,7)
    @BeforeGroups("changeInfo")
    public void selectMyInfoTab() {
        searchEmployeePage().selectMyInfoTab();
    }

    //открытие нужной вкладки перед группой тестов, которые работают на этой вкладке (8,9,10)
    @BeforeGroups("selectEmployees")
    public void selectPimTab() {
        parameterInfoPage().selectPimTab();
    }
}
