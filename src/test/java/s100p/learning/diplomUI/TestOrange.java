package s100p.learning.diplomUI;

import io.qameta.allure.Description;
import org.testng.annotations.Test;
import s100p.learning.diplomUI.supportive.PageObjectSupplier;

public class TestOrange extends BaseTest implements PageObjectSupplier {
    //Тест работает, просто его функционал встроен в базовый метод и для этого теста просто необходимо запустить базовый тест, который и так запустится по Beforesuite. Логично было вынести функционал входа на сайт в базовый метод, т.к. для каждого теста отдельно всегда будет требоваться вход на сайт. Но и отдельный тест входа для отчета тоже нужен. НЕ РЕКОМЕДУЕТСЯ ТАК ДЕЛАТЬ
    @Description("№1 Проверка ввода логина и пароля")
    @Test(priority = 1)
    public void logIn() {
    }

    @Description("№2 Проверка поиска вкладки time")
    @Test(priority = 2)
    public void checkItemPage() {
        searchTimeTabPage()
                .searchField("time")
                .checkTimeItemTab();
    }

    @Description("№3 Негативный тест. Проверка получения ошибки при попытки поиска несуществующего сотрудника")
    @Test(priority = 3, groups = "searchEmployee")
    public void searchFakeEmployee() {
        searchEmployeePage()
                .insertFakeName()
                .pushViewButtonNoResult()
                .getError()
                .searchFieldClear();
    }

    @Description("№4 Негативный тест. Проверка получения ошибки при попытки поиска существующего сотрудника, но без использования автокомлита")
    @Test(priority = 4, groups = "searchEmployee")
    public void searchEmployeeWithNoAutocomplete() {
        searchEmployeePage()
                .insertName()
                .pushViewButtonNoResult()
                .getError()
                .searchFieldClear();
    }

    @Description("№5 Проверка поиска существующего сотрудника с использованием автокомлита")
    @Test(priority = 5, groups = "searchEmployee")
    public void searchEmployeeWithAutocomplete() {
        searchEmployeePage()
                .insertName();
        searchEmployeePage()
                .takeAutocomplete()
                .pushViewButtonWithResult();
    }

    @Description("№6 Проверка возможности ввода новых данных сотрудника - ФИО")
    @Test(priority = 6, groups = "changeInfo")
    public void setEmployeeName() {
        parameterInfoPage().setFullName("Test63", "Brown", "Collings");
    }

    //Понимаю, что dependsOn антипаттерн, но тут он выглядит обоснованным, т.к. текущему тесту просто необходимы действия в предшествующем тесте
    @Description("№7 Проверка функциональности сохранения изменений и получения подтвеждения")
    @Test(priority = 7, groups = "changeInfo", dependsOnMethods = "setEmployeeName")
    public void saveChanges() {
        parameterInfoPage()
                .pushSaveButton()
                .getSaveConfirmation();
    }

    @Description("№8 Проверка функциональности выбора сотрудников через checkBoxes по следующему сценарию: выбрать первых четырех сотрудников")
    @Test(priority = 8, groups = "selectEmployees")
    public void selectEmployeesCheckBoxes() {
        deleteEmployeesPage().selectEmployees(4);
    }

    @Description("№9 Проверка присутвия кнопки delete selected и фунциональности нажатия на эту кнопку")
    @Test(priority = 9, groups = "selectEmployees")
    public void deleteSelectedEmployees() {
        deleteEmployeesPage()
                .getEmployeeList()
                .get(2)
                .click();
        deleteEmployeesPage()
                .checkDeleteButton()
                .pushDeleteButton();
    }


    //Понимаю, что dependsOn антипаттерн, но тут он выглядит обоснованным, т.к. текущему тесту просто необходимы действия в предшествующем тесте
    @Description("№10 Проверка присутствия кнопки предупреждения и функциональности нажатия на кнопку yes")
    @Test(priority = 10, groups = "selectEmployees", dependsOnMethods = "deleteSelectedEmployees")
    public void confirmDeletion() {
        deleteEmployeesPage()
                .checkAlertButton()
                .pushYesButton();
    }


}


