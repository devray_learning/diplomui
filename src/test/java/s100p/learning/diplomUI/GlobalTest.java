package s100p.learning.diplomUI;

import org.testng.annotations.Test;
import s100p.learning.diplomUI.supportive.PageObjectSupplier;

public class GlobalTest extends BaseTest implements PageObjectSupplier {
    //Глобал тест, по сути, это запуск всех тестов из TestOrange разом с некоторыми облегчениями
    @Test()
    public void globalTest() {
        searchTimeTabPage().
                pickItemPage("time");
        searchEmployeePage()
                .searchEmployees()
                .selectMyInfoTab();
        parameterInfoPage()
                .setFullName("Test63", "Brown", "Collings")
                .pushSaveButton()
                .getSaveConfirmation()
                .selectPimTab();
        deleteEmployeesPage()
                .selectEmployees(4).deleteSelectedEmployees()
                 .confirmDeletion();
    }
}
