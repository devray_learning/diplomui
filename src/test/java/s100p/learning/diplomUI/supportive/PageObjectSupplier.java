package s100p.learning.diplomUI.supportive;

import s100p.learning.diplomUI.pages.*;


public interface PageObjectSupplier {

    default LoginPage loginPage () {
        return new LoginPage();
    }

    default SearchTimeTabPage searchTimeTabPage() {
        return new SearchTimeTabPage();
    }

    default SearchEmployeePage searchEmployeePage() {
        return new SearchEmployeePage();
    }

    default ParameterInfoPage parameterInfoPage() {
        return new ParameterInfoPage();
    }

    default DeleteEmployeesPage deleteEmployeesPage() {
        return new DeleteEmployeesPage();
    }

}
