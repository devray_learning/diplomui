package s100p.learning.diplomUI.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ParameterInfoPage {

    private final SelenideElement firstNameField = $x("//input[@placeholder='First Name']");
    private final SelenideElement middleNameField = $x("//input[@placeholder='Middle Name']");
    private final SelenideElement lastNameField = $x("//input[@placeholder='Last Name']");
    private final SelenideElement saveButton = $x("//div[@class='orangehrm-horizontal-padding orangehrm-vertical-padding']//button[@type='submit'][normalize-space()='Save']");
    private final SelenideElement saveSuccessConfirmation = $x("//div[@class='oxd-toast-content oxd-toast-content--success']");
    private final SelenideElement pimTab = $x("//span[normalize-space()='PIM']");


    @Step("Очистить поле от образца и ввести firstName: {firstName}")
    public ParameterInfoPage setFirstName(String firstName) {
        firstNameField.sendKeys(Keys.CONTROL + "A");
        firstNameField.sendKeys(Keys.BACK_SPACE);
        firstNameField.sendKeys(firstName);
        return this;
    }

    @Step("Очистить поле от образца и ввести middleName: {middleName}")
    public ParameterInfoPage setMiddleName(String middleName) {
        middleNameField.sendKeys(Keys.CONTROL + "A");
        middleNameField.sendKeys(Keys.BACK_SPACE);
        middleNameField.sendKeys(middleName);
        return this;
    }

    @Step("Очистить поле от образца и ввести lastName: {lastName}")
    public ParameterInfoPage setLastName(String lastName) {
        lastNameField.sendKeys(Keys.CONTROL + "A");
        lastNameField.sendKeys(Keys.BACK_SPACE);
        lastNameField.sendKeys(lastName);
        return this;
    }

    @Step("Нажать кнопку save")
    public ParameterInfoPage pushSaveButton() {
        saveButton.click();
        return this;
    }

    @Step("Получить подтверждения сохранения")
    public ParameterInfoPage getSaveConfirmation() {
        saveSuccessConfirmation.should(visible, exist);
        return this;
    }

    @Step("Перейти на вкладку PIM")
    public void selectPimTab() {
        pimTab.click();
    }

    @Step("Ввести full name: {firstName}, {middleName}, {lastName}")
    public ParameterInfoPage setFullName(String firstName, String middleName, String lastName) {
        setFirstName(firstName).setMiddleName(middleName).setLastName(lastName);
        return this;
    }

}
