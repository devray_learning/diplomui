package s100p.learning.diplomUI.pages;

import com.codeborne.selenide.SelenideElement;
import com.github.javafaker.Faker;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class SearchEmployeePage {

    private final SelenideElement searchField = $x("//input[@placeholder='Type for hints...']");
    private final SelenideElement nameItem = $x("//p[@class='oxd-userdropdown-name']");

    //т.к. элемент содержит каждый раз разное имя сотрудника, то забираем текст этого элемента, который в xpath элемента и является именем сотрудника, но автокомлит срабатывает только при вводе первой части имени,поэтому берем только перый элемент массива
    private final String [] nameArray = nameItem.getText().split("\\s");
    private final String name = nameArray[0];

    private final SelenideElement autocomplete = $x("//div[@role='listbox']");
    private final String fakeName = new Faker().name().name();

    private final SelenideElement errorItem = $x("//span[@class='oxd-text oxd-text--span oxd-input-field-error-message oxd-input-group__message']");

    private final SelenideElement viewButton = $x("//button[@type='submit']");

    private final SelenideElement myInfoTab = $x("//span[normalize-space()='My Info']");

    /*
       либо можно таким образом, если выпадающий элемент не получается найти
        searchField.sendKeys(Keys.ARROW_DOWN);
        searchField.sendKeys(Keys.ENTER);
        */
    @Step("Выбрать форму автозаполнения")
    public SearchEmployeePage takeAutocomplete() {
        autocomplete.shouldBe(visible);
        autocomplete.click();
        return this;
    }

    @Step("Ввести имя сотрудника fakeName")
    public SearchEmployeePage insertFakeName() {
        searchField.sendKeys(fakeName);
        return this;
    }

    @Step("Получить ошибку")
    public SearchEmployeePage getError() {
        errorItem.shouldBe(exist, visible);
        return this;
    }

    @Step("Нажать кнопку view без результата")
    public SearchEmployeePage pushViewButtonNoResult() {
        viewButton.shouldBe(visible).click();
        viewButton.shouldNot(disappear);
        return this;

    }

    @Step("Ввести имя сотрудника из шапки сайта")
    public SearchEmployeePage insertName() {
        searchField.sendKeys(name);
        return this;
    }

    @Step("Нажать кнопку view, получить результат")
    public void pushViewButtonWithResult() {
        viewButton.click();
        viewButton.should(disappear);
    }

    //пришлось создать свой такой метод, ибо встроенный searchField.clear() не работает
    @Step("Очистить поле searchField")
    public void searchFieldClear() {
        searchField.sendKeys(Keys.CONTROL + "A");
        searchField.sendKeys(Keys.BACK_SPACE);
    }

    //тут два негативных ожидания, второй на появление ошибки, т.к. сотрудника поставлять в поле надо только через автозаполнение
    @Step("Попробовать найти работников, в том числе с использованием автокомлита (пп 5-15 TestTask)")
    public SearchEmployeePage searchEmployees() {
        insertFakeName()
                .pushViewButtonNoResult()
                .getError()
                .searchFieldClear();
        insertName()
                .pushViewButtonNoResult()
                .getError()
                .searchFieldClear();
        insertName().
                takeAutocomplete()
                .pushViewButtonWithResult();
        return this;
    }

    @Step("Перейти на вкладку MyInfo")
    public void selectMyInfoTab() {
        myInfoTab.click();
    }


}

