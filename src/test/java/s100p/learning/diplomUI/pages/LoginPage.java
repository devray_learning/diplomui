package s100p.learning.diplomUI.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {

    private final SelenideElement loginField = $x("//input[@name='username']");
    private final SelenideElement passwordField = $x("//input[@name='password']");
    private final SelenideElement loginButton = $x("//button[@class='oxd-button oxd-button--medium oxd-button--main orangehrm-login-button']");


    @Step("Проверить присутствие и видимость полей Login и Password")
    public LoginPage checkFields() {
        loginField.shouldBe(exist,visible);
        passwordField.shouldBe(exist,visible);
        return this;
    }
    /* чтобы мои environmentVariables принимал и мавен, я попробовал добавить их и для стадии test отдельно и даже через settings/maven/runner, но не работает, в итоге вписал конструкцию ниже в pom.xml как временную заглушку просто, чтобы пока работала команда mvn clean test
     <environmentVariables>
                            <UI_LOGIN>Admin</UI_LOGIN>
                            <UI_PASSWORD>admin123</UI_PASSWORD>
                        </environmentVariables>
    */
    @Step("Ввести login и password")
    public void insertLoginAndPassword(String login, String password) {
        loginField.sendKeys(System.getenv(login));
        passwordField.sendKeys(System.getenv(password));
        loginButton.click();
    }


    public void logIn() {
        checkFields().insertLoginAndPassword("UI_LOGIN", "UI_PASSWORD");

    }

}
