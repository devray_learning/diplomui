package s100p.learning.diplomUI.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;



public class SearchTimeTabPage {
    //привязано к placeholder, а значит не сработает на др языке, можно через соседний элемент и following sibling
    private final SelenideElement searchField = $x("//input[@placeholder='Search']");
    private final SelenideElement timeTab = $x("//span[normalize-space()='Time']");

    @Step("Ввести в поле поиск слово time")
    public SearchTimeTabPage searchField(String item) {
        searchField.sendKeys(item);
        return this;
    }

    @Step("Проверить присутствие и видимость выпадающей вкладки time")
    public SearchTimeTabPage checkTimeItemTab() {
        timeTab.shouldBe(visible,exist);
        return this;
    }

    @Step("Нажать на выпавшую вкладку time")
    public void selectTimeTab() {
        timeTab.click();
    }

    @Step("Переключиться на вкладку time")
    public void pickItemPage (String item) {
        searchField(item).checkTimeItemTab().selectTimeTab();
    }

}
