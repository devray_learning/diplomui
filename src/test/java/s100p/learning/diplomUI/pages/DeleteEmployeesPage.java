package s100p.learning.diplomUI.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.Getter;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Getter
public class DeleteEmployeesPage {

    private final SelenideElement deleteButton = $x("//button[normalize-space()='Delete Selected']");
    private final SelenideElement deletionAlert = $x("//div[@role='document']");
    private final SelenideElement deletionConfirmation = $x("//button[normalize-space()='Yes, Delete']");
    private final ElementsCollection employeeList = $$x("//i[@class='oxd-icon bi-check oxd-checkbox-input-icon']");

    //По какой-то причине пропускается, следующий за первым выбранным, checkbox в списке. Если намеренно пропустить второй checkbox, пропустится уже третий, т.е. пропуст идет именно следующего за первым выбранным.
    @Step("Выбрать  сотрудников: '{number}'")
    public DeleteEmployeesPage selectEmployees(int number) {
        for (int i = 1; i <= number; i++) {
                employeeList.get(i).click();
                //тут слип просто для удобства просмотра
               // Selenide.sleep(500);
        }
        return this;
    }

    @Step("Проверить присутствие кнопки delete selected")
    public DeleteEmployeesPage checkDeleteButton() {
        deleteButton.shouldBe(visible,exist);
        return this;
    }

    @Step("Нажать кнопку  delete selected")
    public DeleteEmployeesPage pushDeleteButton() {
        deleteButton.click();
        return this;
    }

    @Step("Проверить присутствие кнопки delete selected и нажать эту кнопку")
    public DeleteEmployeesPage deleteSelectedEmployees() {
        checkDeleteButton().pushDeleteButton();
        return this;
    }

    @Step("Проверить присутствие предупреждения")
    public DeleteEmployeesPage checkAlertButton() {
        deletionAlert.shouldBe(exist, visible);
        return this;
    }

    @Step("Нажать кнопку yes")
    public DeleteEmployeesPage pushYesButton() {
        deletionConfirmation.click();
        return this;
    }

    @Step("Проверить присутствие предупреждения и нажать кнопку yes")
    public void confirmDeletion() {
        deletionAlert.shouldBe(exist, visible);
        deletionConfirmation.click();
    }


}
